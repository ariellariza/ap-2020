package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {

    private String alias;

    public KnightAdventurer(){
        this.alias = "Knight";
        this.setAttackBehavior(new AttackWithSword());
        this.setDefenseBehavior(new DefendWithArmor());
    }

    public String getAlias(){
        return this.alias;
    }
}
