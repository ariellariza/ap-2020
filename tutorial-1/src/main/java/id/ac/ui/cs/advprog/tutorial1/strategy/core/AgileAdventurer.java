package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {

    private String alias;

    public AgileAdventurer(){
        this.alias = "Agile";
        this.setAttackBehavior(new AttackWithGun());
        this.setDefenseBehavior(new DefendWithBarrier());
    }

    public String getAlias(){
        return this.alias;
    }
}
