package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    @Override
    public String attack() {
        return "Serang dengan sihir";
    }

    @Override
    public String getType() {
        return "sihir";
    }
}
