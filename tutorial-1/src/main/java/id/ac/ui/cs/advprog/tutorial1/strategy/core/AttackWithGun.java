package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
    @Override
    public String attack() {
        return "Serang dengan senjata api";
    }

    @Override
    public String getType() {
        return "senjata api";
    }
}
