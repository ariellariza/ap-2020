package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    ArrayList<Spell> spellList = new ArrayList<>();
    public ChainSpell(ArrayList<Spell> spell){
        this.spellList = spell;
    }

    @Override
    public void undo() {
        for(int i = spellList.size()-1;i >= 0;i--){
            spellList.get(i).undo();
        }
    }

    @Override
    public void cast() {
        for(Spell i:spellList){
            i.cast();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
